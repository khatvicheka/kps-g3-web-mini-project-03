import Menu from './components/pages/Menu';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import HomeArticle from './components/aritcle/HomeArticle';
import Category from './components/category/Category';
import React, { Component } from 'react'
import strings from './components/locale/string'
import AddArticle from './components/aritcle/AddArticle';
import ViewArticle from './components/aritcle/ViewArticle';
import 'bootstrap/dist/css/bootstrap.min.css';
export default class App extends Component {

  onChangeLanguage = (str) => {
    strings.setLanguage(str);
    this.setState({});
  }

  render() {
    return (
      <div>
        <Router>
          <Menu onChangeLanguage={this.onChangeLanguage} />
          <Switch>
            <Route path='/' exact component={HomeArticle} />
            <Route path='/new-article' component={AddArticle} />
            <Route path='/Category' component={Category} />
            <Route path='/View/:id' component={ViewArticle} />
            <Route path='*' render={() => <div className="container my-5"><h1>404 Page Not Found</h1></div>} />
          </Switch>
        </Router>
      </div>
    )
  }
}


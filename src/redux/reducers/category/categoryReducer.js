import {
    GET_CATEGORYS,
    DELTE_CATEGORY,
} from '../../actions/category/categoryActionType'

const defaultState = {
    category: [],
}
export const categoryReducer = (state = defaultState, action) => {
    switch (action.type) {
        case GET_CATEGORYS: {
            return { category: action.payLoad }
        }
        case DELTE_CATEGORY:
            return { category: state.category.filter((c) => c._id !== action.payLoad) }
        default:
            return state
    }
}
import 
{
    GET_ARTICLES,
    GET_MORE_ARTICLES,
    DETELE_ARTICLE,
    SEARCH_ARTICLE,
    FILTER_ARTICLE_BY_CATEGORY,
}  from '../../actions/article/articleActionType'

const defaultState = {
    dataArticles: [],
    filterArticle: []
}
const articleReducer = (state = defaultState, action) => {
    switch (action.type) {
        case GET_ARTICLES:
            return { ...state,  dataArticles: [...action.payLoad] }
        case GET_MORE_ARTICLES:
            return { ...state,  dataArticles: [...state.dataArticles, ...action.payLoad] }
        case DETELE_ARTICLE:
            return { ...state,  dataArticles: state.dataArticles.filter((data) => data._id !== action.payLoad) }
        case SEARCH_ARTICLE:
            return { ...state,  dataArticles: action.payLoad }
        case FILTER_ARTICLE_BY_CATEGORY: {
            let filterArticle = state.dataArticles.filter((data) => (data.category ? data.category._id : null) === action.payLoad)
            return { ...state, filterArticle: filterArticle }
        }
        default:
            return state
    }
}
export default articleReducer
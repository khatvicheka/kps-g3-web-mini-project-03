import { 
    GET_CATEGORYS,
    DELTE_CATEGORY
} from './categoryActionType'
import axios from 'axios'
import { baseURL } from '../../../config/url'


// -------------------------------------------------------------------
//                  Get categories
// -------------------------------------------------------------------
export const getCategory = () => {
    const innnerGetCategory = async (dispatch)=>{
        const result = await axios.get(`${baseURL}/category`)
        dispatch({
            type:  GET_CATEGORYS,
            payLoad: result.data.data
        })
    }
    return innnerGetCategory
}


// -------------------------------------------------------------------
//                  delete category
// -------------------------------------------------------------------
export const deleteCategory = (id) => {
    const innnerDeleteCategory = async (dispatch)=>{
        await axios.delete(`${baseURL}/category/${id}`)
        dispatch({
            type:   DELTE_CATEGORY,
            payLoad: id
        })
    }
    return innnerDeleteCategory
}

// -------------------------------------------------------------------
//                  edit categories
// -------------------------------------------------------------------
export const editCategory = (id, dataCategory, callBack) => {
    axios.put(`${baseURL}/category/${id}`, dataCategory).then(res => {
        callBack(res)
    })
}


// -------------------------------------------------------------------
//                  add categories
// -------------------------------------------------------------------
export const addCategory = (dataCategory, callBack) => {
    axios.post(`${baseURL}/category`, dataCategory).then((res) => {
        callBack(res)
    })
}
import React from "react";
import strings from "../locale/string";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";

function Card({ article, deleteArticle }) {
  return (
    <div>
      <div className="row">
        <div className="col-md-4">
          <img
            className="img-fluid rounded mb-3 mb-md-0"
            src={
              article.image
                ? article.image
                : "https://lunawood.com/wp-content/uploads/2018/02/placeholder-image.png"
            }
            alt="thumnail"
          />
        </div>
        <div className="col-md-8">
          <h3>{article.title}</h3>
          <p className="text-muted mb-0">
            {strings.createDate} : {convertDate(article.createdAt)}
          </p>
          <p className="text-muted mt-0">
            {strings.category} :{" "}
            {article.category ? article.category.name : "No Type"}
          </p>
          <p>{article.description}</p>
          <br />
          <Link to={`/View/${article._id}`}>
            <Button variant="primary" size="md">
              {strings.view}
            </Button>
          </Link>
          <Link to={`/new-article?update=true&&id=${article._id}`}>
            <Button variant="warning" className="mx-1" size="md">
              {strings.edit}
            </Button>
          </Link>
          <Button
            variant="danger"
            size="md"
            onClick={() => deleteArticle(article._id)}
          >
            {strings.delete}
          </Button>
        </div>
      </div>
      <hr />
    </div>
  );
}


// -------------------------------------------------------------------
//                  Conver data
// -------------------------------------------------------------------
export const convertDate = (dateTime) => {
  const date = new Date(dateTime);
  const year = date.getFullYear();
  const month = date.getMonth();
  const day = date.getDate();
  const dates = [year, month, day];
  return dates.join("-");
};
export default Card;
